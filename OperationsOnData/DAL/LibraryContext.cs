﻿using Microsoft.AspNet.Identity.EntityFramework;
using OperationsOnData.Interfaces;
using OperationsOnData.Models;
using System.Data.Entity;

namespace OperationsOnData.DAL
{
    public class LibraryContext : IdentityDbContext, ILibraryContext
    {
        public LibraryContext() : base("Library")
        {

        }

        public static LibraryContext Create()
        {
            return new LibraryContext();
        }

        public DbSet<Book> Books { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
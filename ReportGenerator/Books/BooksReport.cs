﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using OperationsOnData.Models;
using ReportGenerator.Interfaces;
using System.Collections.Generic;
using System.IO;

namespace ReportGenerator.Books
{
    public class BooksReport : IBooksReportGenerator
    {
        int _totalColumn = 2;
        Document _document;
        Font _fontStyle;
        PdfPTable _table = new PdfPTable(2);
        PdfPCell _cell;
        MemoryStream _memoryStream = new MemoryStream();
        List<Book> _books = new List<Book>();

        public byte[] PrepareBooksReport(List<Book> data)
        {
            _books = data;
            _document = new Document(PageSize.A4, 0f, 0f, 0f, 0f);
            _document.SetPageSize(PageSize.A4);
            _document.SetMargins(20f, 20f, 20f, 20f);
            _table.WidthPercentage = 100;
            _table.HorizontalAlignment = Element.ALIGN_LEFT;
            _fontStyle = FontFactory.GetFont("Tahoma", 8f, 1);
            PdfWriter.GetInstance(_document, _memoryStream);
            _document.Open();
            _table.SetWidths(new float[] { 80f, 80f });

            this.ReportHeader();
            this.ReportBody();
            _table.HeaderRows = 2;
            _document.Add(_table);
            _document.Close();

            return _memoryStream.ToArray();
        }

        private void ReportBody()
        {
            _fontStyle = FontFactory.GetFont("Tahoma", 8f, 1);

            _cell = new PdfPCell(new Phrase("Author", _fontStyle));
            _cell.HorizontalAlignment = Element.ALIGN_CENTER;
            _cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            _cell.BackgroundColor = BaseColor.LIGHT_GRAY;
            _table.AddCell(_cell);

            _cell = new PdfPCell(new Phrase("Title", _fontStyle));
            _cell.HorizontalAlignment = Element.ALIGN_CENTER;
            _cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            _cell.BackgroundColor = BaseColor.LIGHT_GRAY;
            _table.AddCell(_cell);

            _fontStyle = FontFactory.GetFont("Tahoma", 8f, 1);
            foreach(Book book in _books)
            {
                _cell = new PdfPCell(new Phrase(book.Author, _fontStyle));
                _cell.HorizontalAlignment = Element.ALIGN_CENTER;
                _cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                _cell.BackgroundColor = BaseColor.WHITE;
                _table.AddCell(_cell);

                _cell = new PdfPCell(new Phrase(book.Title, _fontStyle));
                _cell.HorizontalAlignment = Element.ALIGN_CENTER;
                _cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                _cell.BackgroundColor = BaseColor.WHITE;
                _table.AddCell(_cell);
            }
        }

        private void ReportHeader()
        {
            _fontStyle = FontFactory.GetFont("Tahoma", 11f, 1);
            _cell = new PdfPCell(new Phrase("Books Raport", _fontStyle));
            _cell.Colspan = _totalColumn;
            _cell.HorizontalAlignment = Element.ALIGN_CENTER;
            _cell.Border = 0;
            _cell.BackgroundColor = BaseColor.WHITE;
            _cell.ExtraParagraphSpace = 0;
            _table.AddCell(_cell);
            _table.CompleteRow();

            _fontStyle = FontFactory.GetFont("Tahoma", 9f, 1);
            _cell = new PdfPCell(new Phrase("List of books", _fontStyle));
            _cell.Colspan = _totalColumn;
            _cell.HorizontalAlignment = Element.ALIGN_CENTER;
            _cell.Border = 0;
            _cell.BackgroundColor = BaseColor.WHITE;
            _cell.ExtraParagraphSpace = 0;
            _table.AddCell(_cell);
            _table.CompleteRow();
        }
    }
}

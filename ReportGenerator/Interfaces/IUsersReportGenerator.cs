﻿using OperationsOnData.Models;
using System.Collections.Generic;

namespace ReportGenerator.Interfaces
{
    public interface IUsersReportGenerator
    {
        byte[] PrepareUsersReport(List<User> data);
    }
}

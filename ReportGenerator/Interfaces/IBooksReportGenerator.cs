﻿using OperationsOnData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportGenerator.Interfaces
{
    public interface IBooksReportGenerator
    {
        byte[] PrepareBooksReport(List<Book> data);
    }
}
